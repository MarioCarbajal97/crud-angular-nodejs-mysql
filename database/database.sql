CREATE DATABASE empleados_db;

USE empleados_db;

CREATE TABLE empleado(
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(25),
    last_name VARCHAR(25),
    email VARCHAR(40),
    phone VARCHAR(25)
)