"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class EmpleadosController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const empleados = yield database_1.default.query('SELECT * FROM empleado');
            console.log(empleados);
            res.json(empleados);
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const empleado = yield database_1.default.query('SELECT * FROM empleado WHERE id = ?', [id]);
            if (empleado.length > 0) {
                console.log(empleado);
                return res.json(empleado[0]);
            }
            res.status(404).json({ text: "El empleado no existe" });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.body);
            yield database_1.default.query('INSERT INTO empleado set ?', [req.body]);
            res.json({ message: 'Empleado guardado!' });
        });
    }
    delete(req, res) {
        const { id } = req.params;
        database_1.default.query('DELETE FROM empleado WHERE id = ?', [id]);
        res.json({ message: 'Empleado eliminado!' });
    }
    update(req, res) {
        const { id } = req.params;
        database_1.default.query('UPDATE empleado set ? WHERE id = ?', [req.body, id]);
        res.json({ message: 'Empleado actualizado!' });
    }
}
const empleadosController = new EmpleadosController();
exports.default = empleadosController;
