"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const promise_mysql_1 = __importDefault(require("promise-mysql"));
const keys_1 = __importDefault(require("./keys"));
const pool = promise_mysql_1.default.createPool(keys_1.default.database);
pool.getConnection()
    .then(connection => {
    pool.releaseConnection(connection);
    console.log('DB Conected');
});
// pool.getConnection(function(err, connection) {
//     if (err) throw err; // not connected!
//     // Use the connection
//     connection.query('SELECT * FROM empleado', function (error, results, fields) {
//         // When done with the connection, release it.
//         connection.release();
//         console.log('DB Conected'); 
//         // Handle error after the release.
//         if (error) throw error;
//         // Don't use the connection here, it has been returned to the pool.
//     });
// });
exports.default = pool;
