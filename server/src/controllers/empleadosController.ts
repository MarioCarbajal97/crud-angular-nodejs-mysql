import { Request, Response } from "express";
import pool from "../database";

class EmpleadosController {

    public async list (req: Request, res: Response) {
        const empleados = await pool.query('SELECT * FROM empleado');
        console.log(empleados);
        res.json(empleados);
    }

    public async getOne (req: Request, res: Response): Promise<any>{
        const {id} = req.params;
        const empleado = await pool.query('SELECT * FROM empleado WHERE id = ?', [id]);
        if (empleado.length > 0){
            console.log(empleado);
            return res.json(empleado[0]);
        }
        res.status(404).json({text: "El empleado no existe"});
    } 

    public async create (req: Request, res: Response): Promise<void> {
        console.log(req.body);
        await pool.query('INSERT INTO empleado set ?', [req.body]);
        res.json({message:'Empleado guardado!'});
    }

    public delete (req: Request, res: Response) {
        const {id} = req.params;
        pool.query('DELETE FROM empleado WHERE id = ?', [id]);
        res.json({message:'Empleado eliminado!'});
    }

    public update (req: Request, res: Response) {
        const {id} = req.params;
        pool.query('UPDATE empleado set ? WHERE id = ?', [req.body, id]);
        res.json({message:'Empleado actualizado!'});
    }
}

const empleadosController = new EmpleadosController();
export default empleadosController;