import { Request, Response, text } from "express";

class IndexController {

    index (req: Request, res: Response) {
        res.json({text: 'The api is /api/empleados'});
    }
}

export const indexController = new IndexController();