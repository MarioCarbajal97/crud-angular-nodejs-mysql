import { Component, OnInit, HostBinding } from '@angular/core';
import { Empleado } from 'src/app/models/empleado';
import { ActivatedRoute, Router } from '@angular/router';

import { EmpleadosService } from '../../services/empleados.service';

@Component({
  selector: 'app-empleado-form',
  templateUrl: './empleado-form.component.html',
  styleUrls: ['./empleado-form.component.css']
})
export class EmpleadoFormComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  empleado: any = {
    id: 0,
    name: '',
    last_name: '',
    email: '',
    phone: ''
  };

  // tslint:disable-next-line: ban-types
  edit: Boolean = false;

  constructor(
    private empleadosService: EmpleadosService,
    private router: Router,
    private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    const params = this.activeRoute.snapshot.params;
    if(params.id){
      this.empleadosService.getEmpleado(params.id)
      .subscribe(
        res => {
          console.log(res);
          this.empleado = res;
          this.edit = true;
        },
        err => console.error(err)
      );
    }
  }

  saveEmpleado(): void{
    this.empleadosService.saveEmpleado(this.empleado)
    .subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/empleados']);
      },
      err => console.error(err)
    );
    console.log(this.empleado);
  }

  updateEmpleado(): void{
    this.empleadosService.updateEmpleado(this.empleado.id, this.empleado).subscribe(
      res =>{
        console.log(res);
        this.router.navigate(['/empleados']);
      },
      err => console.log(err)
    );
  }

}
