import { Component, OnInit, HostBinding } from '@angular/core';
import { EmpleadosService } from '../../services/empleados.service';

@Component({
  selector: 'app-empleado-list',
  templateUrl: './empleado-list.component.html',
  styleUrls: ['./empleado-list.component.css']
})
export class EmpleadoListComponent implements OnInit {
  @HostBinding('class') classes = 'row';

  empleados: any = [];
  constructor(private empleadoService: EmpleadosService) { }

  ngOnInit(): void {
    this.getEmpleados();
  }

  getEmpleados(){
    this.empleadoService.getEmpleados().subscribe(
      res => {
        this.empleados = res;
      },
      err => console.log(err)
    );
  }

  deleteEmpleado(id: string){
    this.empleadoService.deleteEmpleado(id).subscribe(
      res => {
        console.log(res);
        this.getEmpleados();
      },
      err => console.log(err)
    )
  }

}
