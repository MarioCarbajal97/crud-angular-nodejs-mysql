export interface Empleado {
    id?: number;
    name: string;
    last_name: string;
    email: string;
    phone: string;
}
