import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Empleado } from '../models/empleado';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {
  API_URL = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  getEmpleados(){
    return this.http.get(`${this.API_URL}/empleados`);
  }

  getEmpleado(id: string){
    return this.http.get(`${this.API_URL}/empleados/${id}`);
  }

  saveEmpleado(empleado: Empleado){
    return this.http.post(`${this.API_URL}/empleados`, empleado);
  }

  deleteEmpleado(id: string){
    return this.http.delete(`${this.API_URL}/empleados/${id}`);
  }

  updateEmpleado(id: string | number, empleado: Empleado): Observable<any>{
    return this.http.put(`${this.API_URL}/empleados/${id}`, empleado);
  }
}
